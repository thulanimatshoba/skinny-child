/**
 * WebPack Common
 * @type {webpack}
 */

const glob = require('glob-all');
const path = require('path');
const webpack = require('webpack');


const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PurgecssPlugin = require('purgecss-webpack-plugin');
//const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');




const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    mode: 'development',
    entry: {
        app: './src/index.js',
      },
      output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
      },
      resolve: {
        extensions: ['.js', '.jsx', '.sass', '.vue', '.scss'],
        alias: {
          vue$: 'vue/dist/vue.esm.js',
        },
      },
    module: {
        rules: [
            /** VUE */
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            /** JS/JSX */
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            },
            /** Images */
            {
                test: /\.(png|ico|gif|jpe?g|jpg)(\?[a-z0-9]+)?$/,
                exclude: /node_modules/,
                loader: 'url-loader',
                options: {
                limit: 1000, /** Bytes */
                },
            },
            /** SASS/CSS */
            {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.html$/,
                use: [{
                loader: 'html-loader',
                options: {
                    minimize: true,
                },
                }],
            },
            /** Fonts */
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                exclude: /node_modules/,
                use: ['url-loader'],
            },
        ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebPackPlugin({
        template: "index.html",
        filename: "./index.html"

    }),
    new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFileName: "[id].css"
    }),
    //new ExtractTextPlugin('/css/[name].css'),
    new BrowserSyncPlugin({
        proxy: 'https://demo.local',
        port: 4002,
        files: [
            '**/*.php'
        ],
        ghostMode: {
            clicks: false,
            location: false,
            forms: false,
            scroll: false
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'webpack',
        notify: true,
        reloadDelay: 0
    }),
    new PurgecssPlugin({
        paths: glob.sync([
          path.join(__dirname, '../**/*.php'),
          path.join(__dirname, '../src/**/*.js'),
          path.join(__dirname, '../src/**/*.vue'),
        ]),
      }),
  ],
};