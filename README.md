[![Build Status](https://travis-ci.org/Automattic/_s.svg?branch=master)](https://bitbucket.org/thulanimatshoba/skinny-child/addon/pipelines/home#!/results/1)

_s
===

Hi. I'm the child theme developed by Thulani Matshoba and he named me `skinny-child`, or `Skinny Ninjah Child Theme`, I'm a theme meant for hacking the main skinny-ninjah theme without touching the core files so don't use me as a Parent Theme. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for.



Getting Started
---------------

If you want to keep it simple, head over to http://thulanimatshoba.co.za and ask `Thulani Matshoba` for some assistance.


1. run `'npm i && npm run build && npm run watch'` (inside your theme directory obviously, duh!)

Now you're ready to go! The next step is easy to say, but harder to do: Do some cool shit. :)

Good luck!
