<?php

function skinny_child_enqueue_styles() {
    $parent_style = 'skinny-ninjah';

    wp_enqueue_script( $parent_style, get_stylesheet_directory_uri() . '/dist/app.js', [], THULANI_VER );
}
add_action( 'wp_enqueue_scripts', 'skinny_child_enqueue_styles' );